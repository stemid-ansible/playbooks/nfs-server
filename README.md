# NFS Server

Simple NFS server setup, mostly tested on CentOS 7 as of writing, 2020-04-21.

## Dependencies

A few roles written by me, hosted on [gitlab.com](https://gitlab.com/stemid-ansible/roles).

    $ ansible-galaxy install -r requirements.yml

## Setup in vagrant

### Create VM and run bootstrap.yml to install basic services

    $ vagrant up
    $ vagrant ssh-config > ~/.ssh/vagrant.conf
    $ ansible-playbook -i inventory/default/hosts site.yml
    $ sudo mount.nfs 192.168.122.xx:/var/nfs /mnt

## Setup in your own environment

    $ cp -r inventory/default inventory/myenv

* Edit ``inventory/myenv/group_vars/nfs_servers.yml`` to add exported directories.
* Edit ``inventory/myenv/hosts`` to add your own NFS servers.

    $ ansible-playbook -i inventory/myenv/hosts bootstrap.yml
    $ ansible-playbook -i inventory/myenv/hosts site.yml

## Protip: Put your environment in its own git branch

    $ git checkout -B myenv
    $ git add inventory/myenv
    $ git commit...; git remote add...; git push -u ...

That way you can update this repo and do ``git merge master`` to merge the changes into your own branch.
